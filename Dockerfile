FROM odoo:10.0

MAINTAINER Nadia AFAKROUCH <nadia.afa@gmail.com>



USER root
RUN apt-get update  && \
apt-get install -qy python-setuptools python-dev build-essential && \
easy_install pip


RUN pip install twilio==5.0.0
RUN pip install twilio==5.3.0

RUN pip install twilio==6.3.0


USER odoo